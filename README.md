# Age Balance Analysis



## Description
Tools for analyzing data from dynamic isotopic labelling experiment, based on metabolic ages.

## Authors and acknowledgment
A collaboration between: Elad Noor, Evgeny Onischenko, and Kirill Jefimov.

## License
This project is open-source and licensed under the MIT license.

## Project status
This project is a repository for scientific research and is should by no means be used for any mission-criticial 
purposes. Furthermore, there is no release schedule and probably never will be.
