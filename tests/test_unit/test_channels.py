"""Tests for channels."""
# The MIT License (MIT)
#
# Copyright (c) 2023 Weizmann Institute of Science, Rehovot, Israel.
# Copyright (c) 2023 University of Bergen, Bergen, Norway.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import pytest
import numpy as np
from age_balance_analysis import channels


@pytest.mark.parametrize(
    "c1_values, c2_values, expected_psi",
    [
        (
                [0.1, 0.2, 0.3, 0.4],
                [0.09, 0.18, 0.25, 0.32],
                0.23,
        )
    ]
)
def test_fitting_psi(c1_values, c2_values, expected_psi):
    psi = channels.find_best_fitting_psi(np.array(c1_values), np.array(c2_values),
                                         bounds=(0.1, 1.5))
    assert psi == pytest.approx(expected_psi, rel=0.1)


@pytest.mark.parametrize(
    "c1, c2, psi, expected_projected_c1",
    [
        (
                0.1, np.nan, 0.5, 0.1,
        ),
        (
                np.nan, 0.3, 0.5, 0.35,
        ),
        (
                0.1, 0.3, 0.5, 0.24,
        ),
        (
                0.1, 0.3, 0.2, 0.22,
        )
    ]
)
def test_projecting(c1, c2, psi, expected_projected_c1):
    projected_c1 = channels.project_on_channel_one(c1, c2, psi)
    assert projected_c1 == pytest.approx(expected_projected_c1, rel=0.1)

@pytest.mark.parametrize(
    "c1, c2, psi, expected_projected_c1",
    [
        (
                [0.1, np.nan, 0.1],
                [np.nan, 0.3, 0.3],
                0.5,
                [0.1, 0.35, 0.24],
        ),
    ]
)
def test_projecting_array(c1, c2, psi, expected_projected_c1):
    projected_c1 = channels.project_on_channel_one(np.array(c1), np.array(c2), psi)
    assert projected_c1 == pytest.approx(expected_projected_c1, rel=0.1)
