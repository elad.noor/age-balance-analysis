import sympy
import networkx as nx  # package for plotting networks
from typing import Tuple, List, Union, Optional
import matplotlib.pyplot as plt  # a standard plotting package for python
import numpy as np  # common mathematical functions
from scipy.optimize import curve_fit  # curve fitting function used for fitting the M matrix parameters based on labelling data
from scipy.linalg import expm  # matrix exponent function (used in the calculation of the labelling function)

class SymbolicAgeBalanceAnalysis(object):

    def __init__(self, n_states: int, n_params: int):
        self.n_states = n_states
        self.n_params = n_params

        # 1-vector for labelling computation. See Appendix eq.(7)
        self._ones = np.ones(self.n_states)

        self._params = list(map(lambda i: sympy.Symbol(f"p_{i}"), range(self.n_params)))

        self._p0 = np.ones(self.n_params) * 0.5
        self._lbs = np.zeros(self.n_params, dtype=float)
        self._ubs = np.ones(self.n_params, dtype=float)

        self._contributed_turnover_matrix = sympy.Matrix(np.zeros((self.n_states, self.n_states)))

        # observed pools, fractional distribution - here we only observe the 3rd pool (S2)
        # by default, set the last pool as the only observed one
        self._observed_pool_vector = sympy.Matrix(np.zeros(self.n_states))
        self._observed_pool_vector[-1] = 1.0

    def set_observed_pool_weight(self, i: int, value: Union[float, sympy.Expr]) -> None:
        self._observed_pool_vector[i] = value

    def set_coeff(self, i: int, j: int, value: Union[float, sympy.Expr]) -> None:
        self._contributed_turnover_matrix[i, j] = value

    def get_row_sum(self, i: int):
        return self._contributed_turnover_matrix.row(i).sum()

    def get_params(self) -> List[sympy.Symbol]:
        return self._params

    def get_param(self, i: int) -> sympy.Symbol:
        return self._params[i]

    def set_param_bounds(self, i: int, lb: float, ub: float) -> None:
        self._lbs[i] = lb
        self._ubs[i] = ub
        self._p0[i] = (lb + ub)/2.0

    def get_param_bounds(self, i: int) -> Tuple[float, float]:
        return self._lbs[i], self._ubs[i]

    def display_bounds(self):
        return [
            (
                sympy.LessThan(self._lbs[i], self._params[i]),
                sympy.LessThan(self._params[i], self._ubs[i])
            )
            for i in range(self.n_params)
        ]

    def get_M_lambda(self):
        return sympy.lambdify(self._params, self._contributed_turnover_matrix)

    def get_solution_M(self, x: np.ndarray) -> np.ndarray:
        """Converts the input parameters to M."""
        M_lambda = self.get_M_lambda()
        M = M_lambda(*x)
        return M

    def get_s_lambda(self):
        return sympy.lambdify(self._params, self._observed_pool_vector)

    def get_solution_s(self, x: np.ndarray) -> np.ndarray:
        s_lambda = self.get_s_lambda()
        s = s_lambda(*x)
        return s.flatten()

    def get_f_lambda(self, x: np.ndarray):
        M = self.get_solution_M(x)
        s = self.get_solution_s(x)
        return lambda t: s @ expm(M * t) @ self._ones

    def get_age_lambda(self, x: np.ndarray):
        M = self.get_solution_M(x)
        s = self.get_solution_s(x)
        return lambda t: -s @ M @ expm(M * t) @ self._ones

    def get_lifespan_lambda(self, x: np.ndarray):
        M = self.get_solution_M(x)
        s = self.get_solution_s(x)
        return lambda t: -s @ M @ M @ expm(M * t) @ self._ones

    def get_mean_age(self, x: np.ndarray):
        M = self.get_solution_M(x)
        s = self.get_solution_s(x)
        return -(s @ np.linalg.inv(M) @ self._ones) / (s @ self._ones)

    def get_mean_lifespan(self, x: np.ndarray):
        M = self.get_solution_M(x)
        s = self.get_solution_s(x)
        return -(s @ self._ones) / (s @ M @ self._ones)

    def fit(self, tdata: np.ndarray, ydata: np.ndarray) -> Tuple[np.ndarray, np.ndarray, float]:
        """Fits the model to the data.

        Parameters
        ----------
            tdata : np.ndarray
                The time series.
            ydata : np.ndarray
                The target values.

        Returns
        -------
            popt : np.ndarray
                the best fitting parameters
            pcov : np.ndarray
                the covariance matrix (see docs for scipy.optimize.curve_fit).
            delta_aic : float
                the Aikaike information criterion (ΔAIC)
        """
        M_lambda = self.get_M_lambda()
        s_lambda = self.get_s_lambda()
        n = len(tdata)

        def f(tdata: np.ndarray, *x) -> np.ndarray:
            M = M_lambda(*x)
            s = s_lambda(*x).flatten()
            try:
                pred = [s @ expm(M * t) @ self._ones for t in tdata.flat]
                return np.array(pred)
            except RuntimeError:
                # return f(t) = 1 if labelling computation fails
                return np.ones(n)

        # see Part 3 for optimization function derails
        try:
            result = curve_fit(
                f=f,
                xdata=tdata,
                ydata=ydata,
                p0=self._p0,
                bounds=[self._lbs, self._ubs],
                full_output=False
            )
            popt, pcov = result

            # calculate the Aikaike information criterion (ΔAIC) based on:
            # https://en.wikipedia.org/wiki/Akaike_information_criterion#Comparison_with_least_squares
            rss = sum((ydata - f(tdata, *popt)) ** 2)  # residual sum of squares
            delta_aic = 2 * self.n_params + n * np.log(rss / n)
            return popt, pcov, delta_aic
        except RuntimeError:
            # returns NaNs if optimization fails
            return np.nan * np.ones(self.n_params), np.nan * np.ones((self.n_params, self.n_params)), np.nan

    def draw_transition_matrix(self, ax: plt.Axes, x: np.ndarray, seed: int = 13648) -> None:
        M = self.get_solution_M(x)
        G = nx.Graph()
        edge_labels = {}
        for i in range(M.shape[0]):
            influx = -M[i, :].sum()
            if influx >= 1e-3:
                G.add_edge(f"i{i}", f"s{i}", weight=3)
                edge_labels[(f"i{i}", f"s{i}")] = influx.round(2)
            for j in range(M.shape[1]):
                if i == j:
                    continue
                if np.abs(M[i, j]) < 1e-3:
                    continue
                G.add_edge(f"s{j}", f"s{i}", weight=1)
                edge_labels[(f"s{j}", f"s{i}")] = M[i, j].round(2)

        # instead of the diagonal values, show the row sum as the turnover contributed by the external state

        pos = nx.spring_layout(G, seed=seed)
        nx.draw_networkx_nodes(G, pos=pos, node_size=400, ax=ax)  # draw nodes and edges
        nx.draw_networkx_labels(G, pos=pos, ax=ax)  # draw node labels
        nx.draw_networkx_edges(G, pos=pos, arrowstyle="->", arrowsize=30, arrows=True, width=1, ax=ax)
        nx.draw_networkx_edge_labels(G, pos=pos, edge_labels=edge_labels, ax=ax)  # draw edge weights

    def get_contributed_turnover_matrix(self) -> sympy.Matrix:
        return self._contributed_turnover_matrix

    def draw_trajectory(
            self, ax: plt.Axes, popt: np.ndarray, t_range: Optional[np.ndarray] = None,
            **kwarg
    ) -> None:

        # define the range of time points for plotting
        if t_range is None:
            t_range = np.linspace(start=0, stop=3, num=100)

        f_lysine = self.get_f_lambda(popt)
        ax.plot(t_range, [f_lysine(t) for t in t_range], **kwarg)
        ax.set_xlabel("time")
        ax.set_ylabel("unlabelled fraction")
