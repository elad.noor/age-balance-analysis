"""utility math functions useful for age-balance calculations."""
# The MIT License (MIT)
#
# Copyright (c) 2023 Weizmann Institute of Science, Rehovot, Israel.
# Copyright (c) 2023 University of Bergen, Bergen, Norway.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import matplotlib.pyplot as plt
import scipy.linalg as npla
import numpy as np
from scipy.linalg import inv, expm
import sympy

def eig(A):
    """Sorted eigenvalue decomposition function."""
    eigenValues, eigenVectors = npla.eig(A)
    idx = np.argsort(eigenValues)
    eigenValues = eigenValues[idx]
    eigenVectors = eigenVectors[:, idx]
    return (eigenValues, eigenVectors)


def jordan(A):
    w, U = eig(A)
    J = np.diag(w.real)
    return U, J


def generate_sympy_irreversible(n: int = 4):
    t = sympy.symbols("t")
    tau = sympy.symbols(" ".join([f"τ_{i+1}" for i in range(n)]))

    M = sympy.Matrix(np.zeros((n, n)))
    for i in range(0, n):
        M[i, i] = -1 / tau[i]
    for i in range(1, n):
        M[i - 1, i] = 1 / tau[i]
    g = sympy.Matrix([1 / tau[0]] + [0] * (n - 1)).T

    P, J = M.jordan_form()
    f = g @ P @ J.inv() @ ((J * t).exp() - (J * 0).exp()) @ P.inv()
    gamma = g @ M.pow(-2)
    return t, tau, M, g, P, J, f, gamma


def generate_sympy_reversible(n: int = 4):
    t = sympy.symbols("t")
    y = sympy.symbols("y")  # input flux (into S0)
    s = sympy.symbols(" ".join([f"s_{i}" for i in range(n)]))
    v_fwd = sympy.symbols(" ".join([f"v_f{i+1}" for i in range(n-1)]))
    v_rev = sympy.symbols(" ".join([f"v_r{i+1}" for i in range(n-1)]))

    M = sympy.Matrix(np.zeros((n, n)))
    for i in range(n-1):
        M[i, i+1] = v_fwd[i]
        M[i+1, i] = v_rev[i]
    M -= np.diag(np.sum(M, 0))
    M[0, 0] -= y
    M = M @ sympy.Matrix(np.diag(s)).pow(-1)
    return t, y, s, v_fwd, v_rev, M


def plot_M_solution(
    M: np.array, g: np.array = None, f0: np.array = None, max_t: float = 3.0
):
    n = M.shape[0]

    # if "g" is not give, assume that we have only one input that turns from
    # 0 to 1 at time t=0, and that the first pool has no other inputs.
    if type(g) == np.ndarray:
        beta = -g @ inv(M)
        beta = beta[:, np.newaxis].T
    else:
        beta = np.ones((1, n))

    alpha = beta.copy()
    if type(f0) == np.ndarray:
        alpha -= f0

    n_timepoints = 100
    tspace = np.linspace(0, max_t, n_timepoints)
    specdata = np.repeat(beta, n_timepoints, 0) - np.vstack(
        [alpha @ expm(M * t) for t in tspace]
    )

    fig = plt.figure(figsize=(20, 5))
    ax = fig.add_subplot(1, 3, 1)
    ax.plot(tspace, specdata)
    ax.legend([f"$f_{i}$" for i in range(n + 1)])

    # make a sympy representation of 'f'
    ax = fig.add_subplot(1, 3, (2, 3))
    s_M = sympy.latex(sympy.Matrix(M.round(2)), mode="plain", mat_str="array")
    s_a = sympy.latex(sympy.Matrix(alpha.round(2)), mode="plain", mat_str="array")
    s_b = sympy.latex(sympy.Matrix(beta.round(2)), mode="plain", mat_str="array")
    ax.text(
        0,
        0.5,
        f"$f(t) ={s_b}^\\top - {s_a}^\\top \\exp \\left( {s_M} \Delta t \\right)$",
        ha="left",
        va="center",
        fontsize=12,
    )
    ax.axis("off")

    return fig