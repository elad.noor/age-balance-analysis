"""a module for fitting labelling data."""
# The MIT License (MIT)
#
# Copyright (c) 2023 Weizmann Institute of Science, Rehovot, Israel.
# Copyright (c) 2023 University of Bergen, Bergen, Norway.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import numpy as np
from typing import Tuple, Optional
import pandas as pd
from .util import fit_M, f_labelling
from tqdm import tqdm
from scipy.stats import linregress
import matplotlib.pyplot as plt
import warnings


class KineticStateModel(object):
    DEFAULT_N_POOLS = 2
    DEFAULT_MEASURED_POOL_INDEX = 1
    DEFAULT_MIN_TIME_POINTS = 2
    DEFAULT_MAX_LN_OD_FOR_TURNOVER = 0.3
    DEFAULT_MAX_LN_OD_FOR_PLOTTING = 3.0

    def __init__(
            self,
            initial_pool: Optional[float] = None,
            n_pools: int = DEFAULT_N_POOLS,
            measured_pool_index: int = DEFAULT_MEASURED_POOL_INDEX,
            min_time_points: int = DEFAULT_MIN_TIME_POINTS,
            calc_initial_slope: bool = False,
            max_ln_od_for_init_slope: float = DEFAULT_MAX_LN_OD_FOR_TURNOVER,
            max_ln_od_for_plotting: float = DEFAULT_MAX_LN_OD_FOR_PLOTTING,
    ) -> None:
        """

        :param initial_pool: if given a finite value, the first pool will be fixed-size
        :param n_pools: total number of pools (including the initial one if given)
        :param measured_pool_index: the index of the pool from where labelling is
        measured
        :param calc_initial_slope:
        :param min_time_points: discard samples with less than this number of points (i.e. not NaN values)
        :param max_ln_od_for_init_slope: max. OD cutoff for calculating the initial slope
        :param max_ln_od_for_plotting: max. OD cutoff for plotting the fitting curves
        """
        self.initial_pool = initial_pool
        self.n_pools = n_pools
        self.measured_pool_index = measured_pool_index
        self.calc_initial_slope = calc_initial_slope
        self.min_time_points = min_time_points
        self.max_ln_od_for_turnover = max_ln_od_for_init_slope
        self.max_ln_od_for_plotting = max_ln_od_for_plotting

    def fit_pools(
            self,
            data_df: pd.DataFrame,
    ) -> Tuple[pd.DataFrame, dict, dict]:

        # drop all rows that have less than MIN_TIME_POINTS measured time points
        _df = data_df[
            pd.isnull(data_df).sum(axis=1) < data_df.shape[1] - self.min_time_points
            ]

        # change RuntimeWarning to Exceptions in order to skip cases with bad fits
        warnings.filterwarnings(action="error", category=RuntimeWarning)

        data = []
        popt_dict = {}
        pcov_dict = {}
        for idx in tqdm(data_df.index):
            xdata = data_df.columns.to_numpy()
            ydata = data_df.loc[idx, :].to_numpy()

            # drop all the NaN points
            not_nan = ~np.isnan(ydata)
            xdata = xdata[not_nan]
            ydata = ydata[not_nan]
            n_meas = xdata.shape[0]

            mse = np.nan
            overall_turnover = np.nan
            overall_turnover_sigma = np.nan
            average_age = np.nan
            average_age_sigma = np.nan
            error_msg = ""

            if n_meas > self.min_time_points:
                if self.calc_initial_slope:
                    # use linear regression to fit the initial slope
                    # (for all points measured before the OD reached
                    # self.max_ln_od_for_turnover)
                    r = linregress(
                        [0] + xdata[xdata < self.max_ln_od_for_turnover].tolist(),
                        [1] + ydata[xdata < self.max_ln_od_for_turnover].tolist()
                    )
                    overall_turnover = -r.slope
                    overall_turnover_sigma = r.stderr

                try:
                    popt, pcov, mse = fit_M(xdata, ydata,
                                            measured_pool_index=self.measured_pool_index,
                                            total_n_pools=self.n_pools,
                                            initial_pool=self.initial_pool)
                    average_age = sum(popt)
                    average_age_sigma = float(np.sqrt(sum(sum(pcov))))
                    popt_dict[idx] = popt
                    pcov_dict[idx] = pcov
                except RuntimeWarning as e:
                    error_msg = str(e)
                except RuntimeError as e:
                    error_msg = str(e)
            else:
                error_msg = "not enough measured points"
            data.append((
                idx, self.initial_pool,
                n_meas, self.n_pools, self.measured_pool_index, mse,
                average_age, average_age_sigma,
                overall_turnover, overall_turnover_sigma,
                error_msg
            ))

        # return RuntimeWarning to default behavior
        warnings.filterwarnings(action="default", category=RuntimeWarning)

        return pd.DataFrame(data=data, columns=[
            "index", "lysine_psi",
            "n_measurements", "n_pools", "index_of_measured_pool", "mse",
            "average_age", "average_age_sigma",
            "overall_turnover", "overall_turnover_sigma",
            "error_message",
        ]), popt_dict, pcov_dict

    def plot_fit(
            self, xdata: np.ndarray, ydata: np.ndarray, taus: np.ndarray, ax: plt.Axes,
            label: str = ""
    ) -> None:
        default_taus = [0.01] * self.n_pools
        if self.initial_pool is not None:
            default_taus[0] = self.initial_pool
        default_taus[self.measured_pool_index] = 1.0

        t_range = np.linspace(start=0, stop=self.max_ln_od_for_plotting, num=50)

        average_age = sum(taus)
        ax.set_title(f"mean age = {average_age:.2f}")

        ydata_growth = f_labelling(t_range, self.measured_pool_index, *default_taus)
        ydata_pred = f_labelling(t_range, self.measured_pool_index, *taus)

        ax.scatter(xdata, ydata, color="coral", s=12, label=label)
        ax.plot(t_range, ydata_growth, '--', color="grey", linewidth=1,
                label="growth dilution")
        ax.plot(t_range, ydata_pred, '-', color="navy", linewidth=2, label="KSM fit")
        ax.set_ylim(0, 1)
        ax.set_xlim(0, self.max_ln_od_for_plotting)
        ax.set_ylabel("unlabeled fraction")
        ax.set_xlabel("time [units of 1/$\\mu$]")
        ax.legend()
