"""utility functions for simulating and analysing data with age-dependent
degradation."""
# The MIT License (MIT)
#
# Copyright (c) 2023 Weizmann Institute of Science, Rehovot, Israel.
# Copyright (c) 2023 University of Bergen, Bergen, Norway.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from typing import Callable, Tuple
from tqdm import tqdm
import numpy as np
from scipy.signal import savgol_filter
import matplotlib.pyplot as plt


def simulate_ages(
        f_deg: Callable[[np.ndarray], np.ndarray],
        n_particles: int = 10000,
        t_simulation: float = 10.,
        dt: float = 1e-3,
) -> np.ndarray:
    """Run a stochastic simulation of a single-state model with age-dependent
    degradation.

    :param f_deg:  age-dependent decay function (in 1/sec)
    :param n_particles:  the number of particles in the state
    :param t_simulation:  simulation time (in sec)
    :param dt: time step (in sec)
    :return: an array containing the ages of all the particles in the end
    """
    ages = np.zeros(n_particles, dtype=float)
    n_iter = int(t_simulation / dt)
    for _ in tqdm(range(n_iter)):
        # for each particle, calculate the probability of decay and randomly decide
        # if it decays in this step.
        deg_probability = f_deg(ages) * dt
        ages[deg_probability > np.random.rand(n_particles)] = 0.
        ages += dt

    return ages


def convert_ages_to_labelling(ages: np.ndarray, bins: np.ndarray) -> Tuple[np.ndarray,
np.ndarray]:
    hist, tdata = np.histogram(ages, bins=bins, density=True)
    age_pdf = hist / np.sum(hist)
    fdata = 1 - np.hstack([[0], np.cumsum(age_pdf)])
    return tdata, fdata


def calc_smooth_derivative(tdata: np.ndarray, xdata: np.ndarray) -> np.ndarray:
    dt = tdata[1] - tdata[0]
    return savgol_filter(xdata, window_length=11, polyorder=1, deriv=1) / dt


def plot_derivatives(
        f_deg: Callable[[np.ndarray], np.ndarray],
        n_particles: int = 10000,
        t_simulation: float = 10.,
        dt: float = 1e-3) -> plt.Figure:
    """

    :param f_deg:  age-dependent decay function (result in 1/s)
    :param n_particles:  the number of particle in the simulation
    :param n_iter:  number of iterations
    :return:  a Figure object
    """
    bins = np.linspace(0, 4, 50)

    ages = simulate_ages(f_deg, n_particles=n_particles, t_simulation=t_simulation,
                         dt=dt)
    tdata, unlabelled_fraction = convert_ages_to_labelling(ages, bins=bins)
    first_derivative = calc_smooth_derivative(tdata, unlabelled_fraction)
    second_derivative = calc_smooth_derivative(tdata, first_derivative)

    f_deg_predicted = -second_derivative / first_derivative

    fig, axs = plt.subplots(2, 2, figsize=(6, 6))

    axs[0, 0].plot(tdata, unlabelled_fraction, '-')
    axs[0, 0].set_title("unlabeled fraction")

    axs[0, 1].plot(tdata, first_derivative, '-')
    axs[0, 1].set_title("first derivative")

    axs[1, 0].plot(tdata, second_derivative, '-')
    axs[1, 0].set_title("second derivative")

    axs[1, 1].plot(tdata, f_deg_predicted, '-', label="based on derivatives")
    axs[1, 1].plot(tdata, f_deg(tdata), '-', label="predicted", color="coral")
    axs[1, 1].legend()
    axs[1, 1].set_title("decay")
    axs[1, 1].set_xlim(0.5, 3)
    axs[1, 1].set_ylim(0, 5)
    return fig
