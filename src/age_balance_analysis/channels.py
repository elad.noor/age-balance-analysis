"""a module for fitting channel 1-2 data to find the free AminoAcid pool size."""
# The MIT License (MIT)
#
# Copyright (c) 2023 Weizmann Institute of Science, Rehovot, Israel.
# Copyright (c) 2023 University of Bergen, Bergen, Norway.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from typing import Tuple, Union

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from scipy.optimize import minimize_scalar

from .util import (
    average_distance_both_channels,
    find_minimum_residual_both_channels,
    lambda_channel_one,
    lambda_channel_two,
)


def find_best_fitting_psi(
        c1_values: np.ndarray, c2_values: np.ndarray,
        bounds: Tuple[float, float] = (1e-3, 0.5),
) -> Tuple[float]:
    """Find the Psi value that first the data best.

    :param c1_values: measured channel 1 values
    :param c2_values: measured channel 2 values
    :param bounds: lower and upper bounds on the values of Psi
    :return: the best fit for the value of Psi
    """
    res = minimize_scalar(
        lambda psi: average_distance_both_channels(c1_values, c2_values, psi),
        bounds=bounds,
        method="bounded",
    )
    if not res.success:
        raise ValueError(res.message)

    return res.x


def plot_psi_fitting(
        c1_values: np.ndarray, c2_values: np.ndarray, best_psi: float
) -> plt.Figure:
    """Plot the fitting results and the best Psi trajectory.

    :param c1_values: measured channel 1 values
    :param c2_values: measured channel 2 values
    :param best_psi: the best fit of Psi
    :return: a Figure object
    """

    x_range = np.linspace(start=0, stop=1, num=30)
    c1_traj = lambda_channel_one(x_range, best_psi)
    c2_traj = lambda_channel_two(x_range, best_psi)

    fig, axs = plt.subplots(1, 2, figsize=(12, 6), dpi=100, sharex=True, sharey=True)
    ax = axs[0]
    ax.plot([0, 1], [0, 1], "k--", alpha=0.2)
    ax.scatter(x=c1_values, y=c2_values, s=4, color="navy", alpha=0.3)
    ax.plot(c1_traj, c2_traj, color="coral", label=f"optimal $\\psi$ = {best_psi:.2g}")
    ax.set_xlabel("channel = 1")
    ax.set_ylabel("channel = 2")
    ax.set_title("measured c1 vs. c2")

    ax = axs[1]
    ax.plot([0, 1], [0, 1], "k--", alpha=0.2)
    sns.kdeplot(x=c1_values, y=c2_values, ax=ax, color="navy", alpha=0.3)
    ax.plot(c1_traj, c2_traj, color="coral", label=f"optimal $\\psi$ = {best_psi:.2g}")
    ax.set_xlabel("channel = 1")
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)
    ax.set_title("best $\\psi$ fit")
    ax.legend()

    for i, ax in enumerate(axs.flat):
        ax.text(
            -0.2, 1.05, f"({chr(ord('a') + i)})", fontsize=12, transform=ax.transAxes
        )

    fig.tight_layout()
    return fig


def project_on_channel_one(
        c1: Union[float, np.ndarray],
        c2: Union[float, np.ndarray],
        psi: float,
) -> Union[float, np.ndarray]:
    """Find the closest point on the curve (by Euclidian distance).

    :param c1: measured channel 1 value(s)
    :param c2: measured channel 2 value(s)
    :param psi: the parameter representing the free-amino-acid pool size
    :return: the corresponding (projected) channel 1 value(s)
    """

    if isinstance(c1, float) and isinstance(c2, float):
        # Note that this works also if one of the channels is missing (but not both),
        # because the projection will simply be the point with the exact value of the
        # existing channel measurement (zero distance).
        try:
            projected_c1, _, _ = find_minimum_residual_both_channels(c1, c2, psi)
        except ValueError:
            # in case we are missing both channels, there is nothing we can do except
            # ignore this data point.
            projected_c1 = np.nan
    else:
        assert isinstance(c1, np.ndarray) and isinstance(c2, np.ndarray)
        assert c1.shape == c2.shape
        projected_c1 = np.array(
            [
                project_on_channel_one(_c1, _c2, psi)
                for _c1, _c2 in zip(c1.flat, c2.flat)
            ]
        )

    return projected_c1


def plot_projection(
        c1_values: np.ndarray,
        c2_values: np.ndarray,
        projected_c1_values: np.ndarray,
        best_psi: float,
) -> plt.Figure:
    """Plot the channel 1 data projections.

    :param c1_values: measured channel 1 values
    :param c2_values: measured channel 2 values
    :param projected_c1_values: the projected channel 1 values
    :param best_psi: the best fit of Psi
    :return: a Figure object
    """
    x_range = np.linspace(start=0, stop=1, num=30)
    c1_traj = lambda_channel_one(x_range, best_psi)
    c2_traj = lambda_channel_two(x_range, best_psi)

    fig, axs = plt.subplots(1, 2, figsize=(12, 6), dpi=100, sharex=True, sharey=True)
    ax = axs[0]
    ax.scatter(
        c1_values,
        projected_c1_values,
        s=4,
        color="navy",
        alpha=0.3,
    )
    ax.plot([0, 1], [0, 1], "-", color="coral", linewidth=1)
    ax.set_xlabel("measured channel 1 value")
    ax.set_ylabel("projected channel 1 value")
    ax.set_title("measured c1 vs. projected c1")

    ax = axs[1]
    ax.scatter(
        c2_values,
        projected_c1_values,
        s=4,
        color="navy",
        alpha=0.3,
    )
    ax.plot(c2_traj, c1_traj, "-", color="coral", linewidth=1)
    ax.set_xlabel("measured channel 2 value")
    ax.set_ylabel("projected channel 1 value")
    ax.set_title("measured c2 vs. projected c1")
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)

    for i, ax in enumerate(axs.flat):
        ax.text(
            -0.2, 1.05, f"({chr(ord('a') + i)})", fontsize=12, transform=ax.transAxes
        )

    fig.tight_layout()
    return fig
