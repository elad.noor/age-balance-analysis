"""utility functions useful for age-balance calculations."""

# The MIT License (MIT)
#
# Copyright (c) 2023 Weizmann Institute of Science, Rehovot, Israel.
# Copyright (c) 2023 University of Bergen, Bergen, Norway.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from typing import Tuple, Union, Optional

import numpy as np
from scipy.optimize import minimize_scalar, curve_fit
from scipy.linalg import expm


def lambda_channel_one(x: Union[float, np.ndarray], psi: float) -> float:
    if psi == 1:
        return 1.0 - x * (1.0 - np.log(x))
    elif psi == 2:
        return 1.0 - (2.0 * np.sqrt(x) - x)
    else:
        return 1.0 - psi * x ** (1.0 / psi) / (psi - 1.0) + x / (psi - 1.0)


def lambda_channel_two(x: Union[float, np.ndarray], psi: float) -> float:
    if psi == 1:
        f00 = 2.0 * x - x**2.0
        f11 = 1.0 + 2.0 * x * np.log(x) - x**2.0
    elif psi == 2:
        f00 = x - x * np.log(x)
        f11 = 1.0 - x * np.log(x) - 4.0 * np.sqrt(x) + 3.0 * x
    else:
        f00 = psi * x ** (2.0 / psi) / (psi - 2.0) - 2.0 * x / (psi - 2.0)
        f11 = (
            1.0
            - 2.0 * psi * x ** (1.0 / psi) / (psi - 1.0)
            + psi * x ** (2.0 / psi) / (psi - 2.0)
            - 2.0 * x / (psi - 1.0) / (psi - 2.0)
        )
    return f11 / (f00 + f11)


def residual_channel_one(
    c1: Union[float, np.ndarray], x: Union[float, np.ndarray], psi: float
) -> float:
    if np.isnan(c1):
        return 0.0
    return (lambda_channel_one(x, psi) - c1) ** 2


def residual_channel_two(
    c2: Union[float, np.ndarray], x: Union[float, np.ndarray], psi: float
) -> float:
    if np.isnan(c2):
        return 0.0
    return (lambda_channel_two(x, psi) - c2) ** 2


def find_minimum_residual_both_channels(
    c1: float, c2: float, psi: float
) -> Tuple[float, float, float]:
    """
    Finds the closest point on the curve by minimizing the residual (Eurclidean
    distance).

    :return
    projected_c1 : float

    projected_c2 : float

    residual : float

    """
    if np.isnan(c1) and np.isnan(c2):
        raise ValueError(
            "at least one channel (K1 or K2) must be non NaN to minimize the residual"
        )

    res = minimize_scalar(
        lambda x: residual_channel_one(c1, x, psi) + residual_channel_two(c2, x, psi),
        bounds=(0, 1),
        method="bounded",
    )
    return lambda_channel_one(res.x, psi), lambda_channel_two(res.x, psi), res.fun


def average_distance_both_channels(
    c1_values: np.ndarray, c2_values: np.ndarray, psi: float
) -> float:
    """Calculate the average distance from the channel 1-2 curve.

    :param c1_values: the values from the first channel
    :param c2_values: the values from the second channel
    :param psi: the parameter representing the free-amino-acid pool size
    :return: the average distance of the data points from the channel 1-2 curve
    """
    distances = [
        find_minimum_residual_both_channels(c1, c2, psi)[2]
        for c1, c2 in zip(c1_values, c2_values)
    ]
    return np.mean(distances)


def tau_to_M(taus: np.ndarray) -> np.ndarray:
    return -np.diag(1.0 / taus) + np.diag(1.0 / taus[1:], k=-1)


def f_labelling(tdata: np.ndarray, measured_pool_index: int, *x) -> np.ndarray:
    M = tau_to_M(np.array(x))
    try:
        return np.array([expm(M * t)[measured_pool_index, :].sum() for t in tdata.flat])
    except RuntimeError:
        return np.ones(tdata.shape)


def fit_taus(
    xdata: np.ndarray,
    ydata: np.ndarray,
    measured_pool_index: int,
    tau_lbs: np.ndarray,
    tau_ubs: np.ndarray,
    tau_p0: Optional[np.ndarray] = None,
) -> Tuple[np.ndarray, np.ndarray, float]:

    if tau_p0 is None:
        tau_p0 = np.multiply(tau_lbs, tau_ubs) ** (0.5)

    def f(tdata, *x):
        return f_labelling(tdata, measured_pool_index, *x)

    popt, pcov = curve_fit(
        f=f,
        xdata=xdata,
        ydata=ydata,
        p0=tau_p0,
        bounds=[tau_lbs, tau_ubs],
    )
    mse = np.mean((ydata - f_labelling(xdata, measured_pool_index, *popt)) ** 2)

    return popt, pcov, mse


def fit_M(
    xdata: np.ndarray,
    ydata: np.ndarray,
    measured_pool_index: int,
    total_n_pools: int,
    initial_pool: Optional[float] = None,
    lb: float = 1e-3,
    ub: float = 1e3,
) -> Tuple[np.ndarray, np.ndarray, float]:
    tau_lbs = np.ones(total_n_pools) * lb  # lower bounds
    tau_ubs = np.ones(total_n_pools) * ub  # upper bounds
    if initial_pool is not None:
        assert total_n_pools > 1
        tau_lbs[0] = initial_pool - 1e-5
        tau_ubs[0] = initial_pool + 1e-5

    return fit_taus(xdata, ydata, measured_pool_index, tau_lbs, tau_ubs)


def calc_mean_age_trapezoid(
    xdata: np.ndarray, ydata: np.ndarray, growth_rate: float
) -> float:
    """Calculate the mean age based on a simple linear interpolation.

    :param xdata: vector of measurement times
    :param ydata: vector of unlabelled fractions
    :param growth_rate: the growth rate (calculated independently)
    :return: the approximate mean age (area under the labelling curve)
    """
    # we add (0, 1) as the first time point (assuming no labeling at the beginning)
    t = [0] + xdata.tolist()  # time points
    f = [1] + ydata.tolist()  # labeling data

    mean_age = 0.0
    for i in range(len(t) - 1):
        trapezoid_area = (t[i + 1] - t[i]) * (f[i + 1] + f[i]) / 2.0
        mean_age += trapezoid_area

    # add the tail assuming exponential decay with exp(-growth_rate*t)
    mean_age += f[-1] / growth_rate

    return mean_age
