"""a module for fitting biomass growth data and finding the growth rate."""
# The MIT License (MIT)
#
# Copyright (c) 2023 Weizmann Institute of Science, Rehovot, Israel.
# Copyright (c) 2023 University of Bergen, Bergen, Norway.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import linregress


def find_growth_rates(biomass_csv_fname: str) -> pd.DataFrame:
    # load the OD measurements and calculate the growth rate from them
    od_df = pd.read_csv(biomass_csv_fname, index_col=None)
    od_df["lnOD"] = np.log(od_df.biomass)

    fig, ax = plt.subplots(1, 1, figsize=(5, 5), dpi=100)
    for cond, group_df in od_df.groupby("condition"):
        res = linregress(group_df.time, group_df.lnOD)
        growth_rate = res.slope

        ax.plot(group_df.time, group_df.lnOD, '.',
                label=f"condition = {cond}, μ = {growth_rate:.2g} 1/min")
        ax.plot([0, group_df.time.max()],
                [res.intercept, res.slope * group_df.time.max() + res.intercept], '-',
                label=None, color="black", alpha=0.3)

        print(f"condition:   {cond}")
        print(f"Growth rate:   μ = {growth_rate:.2e} 1/min")
        print(f"Doubling time: ln(2)/μ = {np.log(2) / growth_rate:.1f} min")
        print(f"Average age (all biomass): 1/μ = {1 / growth_rate:.1f} min")
        print("-" * 40)
    ax.legend()
    return od_df